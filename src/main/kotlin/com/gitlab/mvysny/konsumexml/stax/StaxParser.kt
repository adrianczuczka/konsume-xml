package com.gitlab.mvysny.konsumexml.stax

import java.io.Closeable
import java.io.InputStream
import java.lang.RuntimeException
import javax.xml.namespace.QName
import javax.xml.stream.XMLStreamException

enum class StaxEventType {
    StartElement,
    EndElement,
    ProcessingInstruction,
    Characters,
    Comment,
    Space,
    StartDocument,
    EndDocument,
    EntityReference,
    Attribute,
    DTD,
    CData,
    Namespace,
    NotationDeclaration,
    EntityDeclaration
}

/**
 * [close] closes the underlying parser, but does not need to close the underlying input stream.
 */
interface StaxParser : Closeable {
    /**
     * Returns an integer code that indicates the type
     * of the event the cursor is pointing to.
     */
    val eventType: StaxEventType

    /**
     * Return the current location of the processor.
     * If the Location is unknown the processor should return
     * an implementation of Location that returns -1 for the
     * location and null for the publicId and systemId.
     * The location information is only valid until next() is
     * called.
     */
    val location: Location

    /**
     * Returns a QName for the current START_ELEMENT or END_ELEMENT event
     * @return the QName for the current START_ELEMENT or END_ELEMENT event
     * @throws IllegalStateException if this is not a START_ELEMENT or
     * END_ELEMENT
     */
    val elementName: QName

    /**
     * Returns the qname of the attribute at the provided index
     * @param index the position of the attribute
     * @return the QName of the attribute
     * @throws IllegalStateException if this is not a START_ELEMENT or ATTRIBUTE
     */
    fun getAttributeName(index: Int): QName

    /**
     * Returns the count of attributes on this START_ELEMENT,
     * this method is only valid on a START_ELEMENT or ATTRIBUTE.  This
     * count excludes namespace definitions.  Attribute indices are
     * zero-based.
     * @return returns the number of attributes
     * @throws IllegalStateException if this is not a START_ELEMENT or ATTRIBUTE
     */
    val attributeCount: Int

    /**
     * Returns the normalized attribute value of the
     * attribute with the namespace and localName
     * @param namespaceURI the namespace of the attribute
     * @param localName the local name of the attribute, cannot be null
     * @return returns the value of the attribute , returns null if not found
     * @throws IllegalStateException if this is not a START_ELEMENT or ATTRIBUTE
     */
    fun getAttributeValue(namespaceURI: String, localName: String): String?

    /**
     * Returns the current value of the parse event as a string,
     * this returns the string value of a CHARACTERS event,
     * returns the value of a COMMENT, the replacement value
     * for an ENTITY_REFERENCE, the string value of a CDATA section,
     * the string value for a SPACE event,
     * or the String value of the internal subset of the DTD.
     * If an ENTITY_REFERENCE has been resolved, any character data
     * will be reported as CHARACTERS events.
     * @return the current text or null
     * @throws java.lang.IllegalStateException if this state is not
     * a valid text state.
     */
    val text: String?

    /**
     * Get next parsing event - a processor may return all contiguous
     * character data in a single chunk, or it may split it into several chunks.
     * If the property javax.xml.stream.isCoalescing is set to true
     * element content must be coalesced and only one CHARACTERS event
     * must be returned for contiguous element content or
     * CDATA Sections.
     *
     * By default entity references must be
     * expanded and reported transparently to the application.
     * An exception will be thrown if an entity reference cannot be expanded.
     * If element content is empty (i.e. content is "") then no CHARACTERS event will be reported.
     *
     * Given the following XML:
     * ```xml
     * <foo><!--description-->content text<![CDATA[<greeting>Hello</greeting>]]>other content</foo><br></br>
     * ```
     * The behavior of calling next() when being on foo will be:
     * 1. the comment (COMMENT)
     * 2. then the characters section (CHARACTERS)
     * 3. then the CDATA section (another CHARACTERS)
     * 4. then the next characters section (another CHARACTERS)
     * 5. then the END_ELEMENT
     *
     *
     * **NOTE:** empty element (such as `<tag/>`) will be reported
     * with  two separate events: START_ELEMENT, END_ELEMENT - This preserves
     * parsing equivalency of empty element to `<tag></tag>`.
     *
     * This method will throw an IllegalStateException if it is called after [hasNext] returns false.
     *
     * @throws NoSuchElementException if this is called when hasNext() returns false
     * @throws XMLStreamException  if there is an error processing the underlying XML source
     */
    fun next()

    /**
     * Returns true if there are more parsing events and false
     * if there are no more events.  This method will return
     * false if the current state of the XMLStreamReader is
     * END_DOCUMENT
     * @return true if there are more events, false otherwise
     * @throws XMLStreamException if there is a fatal error detecting the next state
     */
    fun hasNext(): Boolean
}

/**
 * @property lineNumber Return the line number where the current event ends,
 * returns -1 if none is available. 1-based (first row has the number of 1).
 * @property columnNumber Return the column number where the current event ends,
 * returns -1 if none is available. 1-based (first column has the number of 1).
 * @property characterOffset Return the byte or character offset into the input source this location
 * is pointing to. If the input source is a file or a byte stream then
 * this is the byte offset into that stream, but if the input source is
 * a character media then the offset is the character offset.
 * Returns -1 if there is no offset available.
 * @property publicId Returns the public ID of the XML or null if not available.
 * @property systemId the system ID, or null if not available
*/
data class Location(val lineNumber: Int, val columnNumber: Int, val characterOffset: Int, val publicId: String?, val systemId: String?) {
    override fun toString() = "line $lineNumber column $columnNumber at $publicId $systemId"
}

/**
 * Produces [StaxParser]s. Automatically chooses the proper implementation depending on the platform we're running on.
 * See [isJavaxXmlStreamAvailable] and [isOrgXmlpullAvailable] for more details.
 */
object StaxParserFactory {
    /**
     * Checks whether the `javax.xml.stream` API is available. It is not available on Androids.
     */
    val isJavaxXmlStreamAvailable = try {
        Class.forName("javax.xml.stream.XMLStreamReader")
        true
    } catch (e: ClassNotFoundException) {
        false
    }

    /**
     * Checks whether the `org.xmlpull` API is available. It is typically available on Androids 8+ and also on JDK
     * when you include either `org.ogce:xpp3:1.1.6`, `kxml2` or similar libraries.
     */
    val isOrgXmlpullAvailable = try {
        Class.forName("org.xmlpull.v1.XmlPullParser")
        true
    } catch (e: ClassNotFoundException) {
        false
    }

    /**
     * The factory producing [StaxParser]. By default tries to use `javax.xml.stream` since that one is also able to expand
     * entity references; if it isn't available then `org.xmlpull` is used.
     *
     * Change this to use any [StaxParser] implementation you need, or to provide different parser configuration etc.
     */
    var factory: (stream: InputStream, systemId: String?) -> StaxParser = { stream, systemId ->
        when {
            isJavaxXmlStreamAvailable -> JavaxXmlStreamStaxParser.create(stream, systemId)
            isOrgXmlpullAvailable -> OrgXmlpullStaxParser.create(stream, systemId)
            else -> throw RuntimeException("No StaX library is available")
        }
    }

    /**
     * Calls [factory] to create new [StaxParser]. The parser will read given [stream]; if you know the origin (absolute
     * name of the file, http source url), pass it as [systemId].
     *
     * Closing the parser will not close the stream, you need to close the stream separately.
     */
    fun create(stream: InputStream, systemId: String? = null): StaxParser = factory(stream, systemId)
}
