package com.gitlab.mvysny.konsumexml

import java.io.Closeable

/**
 * Tries to run given block on a [Closeable]. If the block fails, this closable is closed; if the block succeeds,
 * this closeable is not closed since it's expected that the closeable will be used further.
 */
inline fun <T : Closeable, R> T.andTry(block: (T) -> R): R = try {
    block(this)
} catch (e: Exception) {
    try {
        close()
    } catch (ce: Exception) {
    }
    throw e
}
