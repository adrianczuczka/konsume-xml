package realworldtests.jmnedict

import com.github.mvysny.dynatest.DynaTest
import com.gitlab.mvysny.konsumexml.forceJavaxXmlStreamParser
import kotlin.test.expect

class JmnedictXmlTest : DynaTest({
    beforeEach { forceJavaxXmlStreamParser() }
    test("smoke") {
        val dict = JMnedict.file()
        expect(234) { dict.entry.size }
        expect("foo") { dict.entry[0].trans[0].transDet[0].lang }
    }
})
