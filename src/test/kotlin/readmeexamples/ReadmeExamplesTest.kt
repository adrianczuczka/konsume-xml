package readmeexamples

import com.github.mvysny.dynatest.DynaTest
import com.github.mvysny.dynatest.expectList
import com.gitlab.mvysny.konsumexml.*
import java.util.*
import kotlin.test.expect

data class Example(val index: Int, val text: String) {
    companion object {
        fun xml(k: Konsumer): Example {
            k.checkCurrent("example")
            return Example(k.attributes.getValueInt("index"), k.childText("text"))
        }
    }
}

data class Security(val ssl: Boolean, val keyStore: String) {
    companion object {
        fun xml(k: Konsumer): Security {
            k.checkCurrent("security")
            return Security(k.attributes.getValue("ssl") { it.xmlBoolean() }, k.childText("keyStore"))
        }
    }
}
data class Server(val port: Int, val host: String, val security: Security) {
    companion object {
        fun xml(k: Konsumer): Server {
            k.checkCurrent("server")
            return Server(k.attributes.getValueInt("port"),
                    k.childText("host"),
                    k.child("security") { Security.xml(this) })
        }
    }
}
data class Configuration(val server: Server, val id: Int) {
    companion object {
        fun xml(k: Konsumer): Configuration {
            k.checkCurrent("configuration")
            // the attributes are only accessible straight after entering an element. therefore, we need to
            // read attributes as the first thing, before digging into element contents
            val id = k.attributes.getValueInt("id")
            return Configuration(k.child("server") { Server.xml(this) }, id)
        }
    }
}

data class OptionalExample(val version: Int?, val id: String, val name: String?, val address: String) {
    companion object {
        fun xml(k: Konsumer): OptionalExample {
            k.checkCurrent("optionalExample")
            return OptionalExample(k.attributes.getValueIntOpt("version"),
                    k.attributes.getValue("id"),
                    k.childTextOpt("name"),
                    k.childText("address"))
        }
    }
}

data class PropertyList(val name: String, val list: List<Entry>) {
    companion object {
        fun xml(k: Konsumer): PropertyList {
            k.checkCurrent("propertyList")
            return PropertyList(k.attributes["name"], k.child("list") { children("entry") { Entry.xml(this) } })
        }
        fun inlineXml(k: Konsumer): PropertyList {
            k.checkCurrent("propertyList")
            return PropertyList(k.childText("name"), k.children("entry") { Entry.xml(this) })
        }
    }
}

data class PropertyKeys(val name: String, val keys: List<String>) {
    companion object {
        fun xml(k: Konsumer): PropertyKeys {
            k.checkCurrent("propertyList")
            return PropertyKeys(k.childText("name"), k.children("entry") {
                val key = attributes["key"]
                skipContents()
                key
            })
        }
        fun validatingXml(k: Konsumer): PropertyKeys {
            k.checkCurrent("propertyList")
            return PropertyKeys(k.childText("name"), k.children("entry") {
                val key = attributes["key"]
                childText("value")
                key
            })
        }
    }
}

data class Entry(val key: String, val value: String) {
    companion object {
        fun xml(k: Konsumer): Entry {
            k.checkCurrent("entry")
            return Entry(k.attributes["key"], k.childText("value"))
        }
    }
}

data class Parent(val child: Child) {
    companion object {
        fun xml(k: Konsumer): Parent {
            k.checkCurrent("parent")
            return Parent(k.child("child") { Child.xml(this) })
        }
    }
}

data class Child(val name: String, val street: String) {
    companion object {
        fun xml(k: Konsumer): Child {
            k.checkCurrent("child")
            return Child(k.childText("name"), k.child("address") { childText("street") } )
        }
    }
}

data class DateList(val created: Date, val dates: List<Date>) {
    companion object {
        fun xml(k: Konsumer): DateList {
            k.checkCurrent("dateList")
            return DateList(k.attributes.getValue("created") { it.xmlDateTime().time },
                    k.childrenText("date") { it.xmlDateTime().time }
                    )
        }
    }
}

data class FileSet(val path: String, val include: Set<String>, val exclude: Set<String>) {
    companion object {
        fun xml(k: Konsumer): FileSet {
            k.checkCurrent("fileSet")
            val path = k.attributes["path"]
            val include = mutableSetOf<String>()
            val exclude = mutableSetOf<String>()
            k.children(setOf("include", "exclude")) {
                val pattern = attributes["pattern"]
                (if (name!!.localPart == "include") include else exclude).add(pattern)
            }
            return FileSet(path, include, exclude)
        }
    }
}

class ReadmeExamplesTest : DynaTest({
    runOnAllParsers {
        test("01") {
            val example = """
            <example index="123">
               <text>Example message</text>
            </example>""".konsumeXml().child("example") { Example.xml(this) }
            expect(Example(123, "Example message")) { example }
        }

        test("02") {

            val cfg = """
            <configuration id="1234">
               <server port="80">
                  <host>www.domain.com</host>
                  <security ssl="true">
                     <keyStore>example keystore</keyStore>
                  </security>
               </server>
            </configuration>
        """.trimIndent().konsumeXml().child("configuration") { Configuration.xml(this) }
            expect(Configuration(Server(80, "www.domain.com", Security(true, "example keystore")), 1234)) { cfg }
        }

        test("03") {
            val e = """
            <optionalExample id="10">
               <address>Some example address</address>
            </optionalExample>
        """.trimIndent().konsumeXml().child("optionalExample") { OptionalExample.xml(this) }
            expect(OptionalExample(null, "10", null, "Some example address")) { e }
        }

        test("04") {
            val list = """
            <propertyList name="example">
               <list>
                  <entry key="one">
                     <value>first value</value>
                  </entry>
                  <entry key="two">
                     <value>first value</value>
                  </entry>
                  <entry key="three">
                     <value>first value</value>
                  </entry>
                  <entry key="four">
                     <value>first value</value>
                  </entry>
               </list>
            </propertyList>
        """.trimIndent().konsumeXml().child("propertyList") { PropertyList.xml(this) }
            expect(4) { list.list.size }
        }

        test("05") {
            val list = """
            <propertyList>
               <name>example</name>
               <entry key="one">
                  <value>first value</value>
               </entry>
               <entry key="two">
                  <value>second value</value>
               </entry>
               <entry key="three">
                  <value>third value</value>
               </entry>
            </propertyList>
        """.trimIndent().konsumeXml().child("propertyList") { PropertyList.inlineXml(this) }
            expect(3) { list.list.size }
        }

        test("06") {
            val parent = """
            <parent xmlns="http://domain/parent">
               <pre:child xmlns:pre="http://domain/child">
                  <name>John Doe</name>
                  <address xmlns="">
                      <street>Sin City</street>
                  </address>
               </pre:child>
            </parent>
        """.trimIndent().konsumeXml().child("parent") { Parent.xml(this) }
            expect(Parent(Child("John Doe", "Sin City"))) { parent }
        }

        test("07") {
            val dateList = """
            <dateList created="2002-05-30T09:30:10.5">
                <date>2002-05-30T09:00:00Z</date>
                <date>2002-05-30T09:30:10+06:00</date>
            </dateList>
        """.trimIndent().konsumeXml().child("dateList") { DateList.xml(this) }
        }

        test("08") {
            val fileSet = """
            <fileSet path="/user/niall">
               <include pattern=".*.jar"/>
               <exclude pattern=".*.bak"/>
               <exclude pattern="~.*"/>
               <include pattern=".*.class"/>
               <exclude pattern="images/.*"/>
            </fileSet>
        """.trimIndent().konsumeXml().child("fileSet") { FileSet.xml(this) }
            expect(FileSet("/user/niall", setOf(".*.jar", ".*.class"), setOf(".*.bak", "~.*", "images/.*"))) { fileSet }
            println(fileSet)
        }

        test("09") {
            val xml = """
            <propertyList>
               <name>example</name>
               <entry key="one">
                  <value>first value</value>
               </entry>
               <entry key="two">
                  <value>second value</value>
               </entry>
               <entry key="three">
                  <value>third value</value>
               </entry>
            </propertyList>
        """
            val list = xml.konsumeXml().child("propertyList") { PropertyKeys.xml(this) }
            expectList("one", "two", "three") { list.keys }

            val name = xml.konsumeXml().child("propertyList") {
                val name = childText("name")
                skipContents()
                name
            }
            expect("example") { name }

            val list2 = xml.konsumeXml().child("propertyList") { PropertyKeys.validatingXml(this) }
            expectList("one", "two", "three") { list2.keys }
        }
    }
})
