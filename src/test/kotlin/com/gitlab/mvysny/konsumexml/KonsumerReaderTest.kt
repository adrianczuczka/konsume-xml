package com.gitlab.mvysny.konsumexml

import com.github.mvysny.dynatest.DynaNodeGroup
import com.github.mvysny.dynatest.DynaTest
import com.github.mvysny.dynatest.expectList
import com.gitlab.mvysny.konsumexml.stax.*
import java.io.ByteArrayInputStream
import java.io.InputStream
import javax.xml.stream.XMLInputFactory
import javax.xml.stream.XMLStreamReader
import javax.xml.stream.events.XMLEvent
import kotlin.test.expect

internal fun KonsumerReader.drain(): List<StaxEventType> = generateSequence { if (hasNext()) next() else null } .toList()

class KonsumerReaderTest : DynaTest({
    runOnAllParsers {
        fun String.kreader(): KonsumerReader {
            val stream = byteInputStream()
            return KonsumerReader(StaxParserFactory.create(stream), stream)
        }
        test("smoke") {
            val r: KonsumerReader = """<employee><id>1</id></employee>""".trimIndent().kreader()
            expectList(StaxEventType.StartElement, StaxEventType.StartElement, StaxEventType.Characters, StaxEventType.EndElement,
                    StaxEventType.EndElement, StaxEventType.EndDocument) { r.drain() }
        }
        test("push back") {
            val r: KonsumerReader = """<employee><id>1</id></employee>""".trimIndent().kreader()
            expect(StaxEventType.StartElement) { r.next() }
            expect(StaxEventType.StartElement) { r.next() }
            expect("id") { r.stax.elementName.toString() }
            r.pushBack()
            expect(StaxEventType.StartElement) { r.next() }
            expect("id") { r.stax.elementName.toString() }
            expect(StaxEventType.Characters) { r.next() }
            expect("1") { r.stax.text }
        }
    }
})
