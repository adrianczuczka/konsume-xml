package com.gitlab.mvysny.konsumexml

import com.github.mvysny.dynatest.DynaTest
import com.github.mvysny.dynatest.expectThrows
import java.lang.IllegalArgumentException
import java.lang.IllegalStateException
import java.math.BigDecimal
import kotlin.test.expect

class DataTypesTest : DynaTest({
    group("xmlBoolean()") {
        test("simple conversion") {
            listOf("FALSE", "false", "0").forEach {
                expect(false) { it.xmlBoolean() }
            }
            listOf("TRUE", "true", "1").forEach {
                expect(true) { it.xmlBoolean() }
            }
        }
        test("failed conversion") {
            for (probe in listOf("2", "", "invalid", "-1", "trueish")) {
                expectThrows(IllegalStateException::class, "A boolean value of either 'true', 'false', '0' or '1' was expected, but got '$probe'") {
                    probe.xmlBoolean()
                }
            }
        }
    }
    group("xmlDateTime()") {
        test("simple conversion") {
            expect("2002-05-30T09:00:00") { "2002-05-30T09:00:00".xmlDateTime().formatXMLDateTime(false) }
        }
        test("simple conversion with millis") {
            expect("2002-05-30T09:30:10.500") { "2002-05-30T09:30:10.5".xmlDateTime().formatXMLDateTime(false) }
        }
        test("simple UTC conversion") {
            expect("2002-05-30T09:00:00Z") { "2002-05-30T09:00:00Z".xmlDateTime().formatXMLDateTime() }
        }
        test("simple conversion with zone") {
            val dateTime = "2002-05-30T09:30:10+06:00".xmlDateTime()
            expect("2002-05-30T09:30:10+06:00") { dateTime.formatXMLDateTime() }
            expect(1022729410000) { dateTime.time.time }
        }
        test("simple conversion with zone 2") {
            expect("2002-05-30T09:30:10-06:00") { "2002-05-30T09:30:10-06:00".xmlDateTime().formatXMLDateTime() }
        }
        test("failed conversion") {
            expectThrows(IllegalArgumentException::class, "invalid") {
                "invalid".xmlDateTime()
            }
        }
    }
    group("xmlDate()") {
        test("simple conversion") {
            expect("2002-05-30") { "2002-05-30".xmlDate().formatXMLDate() }
        }
        test("failed conversion") {
            expectThrows(IllegalArgumentException::class, "invalid") {
                "invalid".xmlDate()
            }
        }
    }
    group("xmlTime()") {
        test("simple conversion") {
            expect("09:00:00") { "09:00:00".xmlTime().formatXMLTime(false) }
        }
        test("simple conversion with millis") {
            expect("09:30:10.500") { "09:30:10.5".xmlTime().formatXMLTime(false) }
        }
        test("simple UTC conversion") {
            expect("09:00:00Z") { "09:00:00Z".xmlTime().formatXMLTime() }
        }
        test("simple conversion with zone") {
            expect("09:30:10+06:00") { "09:30:10+06:00".xmlTime().formatXMLTime() }
        }
        test("simple conversion with zone 2") {
            expect("09:30:10-06:00") { "09:30:10-06:00".xmlTime().formatXMLTime() }
        }
        test("failed conversion") {
            expectThrows(IllegalArgumentException::class, "invalid") {
                "invalid".xmlTime()
            }
        }
    }
    group("xmlDecimal") {
        test("valid values") {
            expect("123.456") { "123.456".xmlDecimal().toString() }
            expect("1234.456") { "+1234.456".xmlDecimal().toString() }
            expect("-1234.456") { "-1234.456".xmlDecimal().toString() }
            expect("-0.456") { "-.456".xmlDecimal().toString() }
            expect("-456.0") { "-456.0".xmlDecimal().toString() }
        }
        test("invalid values") {
            listOf("1 234.456", "1234.456E+2", "+ 1234.456", "+1,234.456").forEach {
                expectThrows(IllegalArgumentException::class) {
                    it.xmlDecimal()
                }
            }
        }
    }
    group("xmlFloat") {
        test("valid values") {
            expect(-3E2f) { "-3E2".xmlFloat() }
            expect(4268.22752E11f) { "4268.22752E11".xmlFloat() }
            expect(+24.3e-3f) { "+24.3e-3".xmlFloat() }
            expect(12f) { "12".xmlFloat() }
            expect(3.5f) { "3.5".xmlFloat() }
            expect(Float.NEGATIVE_INFINITY) { "-INF".xmlFloat() }
            expect(-0f) { "-0".xmlFloat() }
            expect(Float.NaN) { "NaN".xmlFloat() }
        }
        test("invalid values") {
            listOf("-3E2.4", "12E", "NAN", "").forEach {
                expectThrows(IllegalArgumentException::class) { it.xmlFloat() }
            }
        }
    }
    group("xmlDouble") {
        test("valid values") {
            expect(-3E2) { "-3E2".xmlDouble() }
            expect(4268.22752E11) { "4268.22752E11".xmlDouble() }
            expect(+24.3e-3) { "+24.3e-3".xmlDouble() }
            expect(12.0) { "12".xmlDouble() }
            expect(3.5) { "3.5".xmlDouble() }
            expect(Double.NEGATIVE_INFINITY) { "-INF".xmlDouble() }
            expect(-0.0) { "-0".xmlDouble() }
            expect(Double.NaN) { "NaN".xmlDouble() }
        }
        test("invalid values") {
            listOf("-3E2.4", "12E", "NAN", "").forEach {
                expectThrows(IllegalArgumentException::class) { it.xmlDate() }
            }
        }
    }
    group("xmlHexBinary") {
        test("valid values") {
            expect("0FB8") { "0FB8".xmlHexBinary().formatXmlHexBinary() }
            expect("0FB8") { "0fb8".xmlHexBinary().formatXmlHexBinary() }
            expect("") { "".xmlHexBinary().formatXmlHexBinary() }
        }
        test("invalid values") {
            expectThrows(IllegalArgumentException::class, "hexBinary needs to be even-length: FB8") {
                "FB8".xmlHexBinary()
            }
        }
    }
    group("xmlBase64Binary") {
        test("valid values") {
            expect("D0507C") { "0FB8".xmlBase64Binary().formatXmlHexBinary() }
            expect("D1F6FC") { "0fb8".xmlBase64Binary().formatXmlHexBinary() }
            expect("") { "".xmlBase64Binary().formatXmlHexBinary() }
            expect("D0507CD05FBD") { "0 FB8 0F+9".xmlBase64Binary().formatXmlHexBinary() }
            expect("D05FB8D0") { "0F+40A==".xmlBase64Binary().formatXmlHexBinary() }
        }
        test("invalid values") {
            listOf("==0F").forEach {
                expectThrows(IllegalArgumentException::class) {
                    it.xmlBase64Binary()
                }
            }
        }
    }
})
