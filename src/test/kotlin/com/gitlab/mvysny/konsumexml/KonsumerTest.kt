package com.gitlab.mvysny.konsumexml

import com.github.mvysny.dynatest.DynaNodeGroup
import com.github.mvysny.dynatest.DynaTest
import com.github.mvysny.dynatest.expectList
import com.github.mvysny.dynatest.expectThrows
import com.gitlab.mvysny.konsumexml.stax.JavaxXmlStreamStaxParser
import com.gitlab.mvysny.konsumexml.stax.OrgXmlpullStaxParser
import com.gitlab.mvysny.konsumexml.stax.StaxParserFactory
import readmeexamples.Resource
import java.lang.IllegalStateException
import javax.xml.XMLConstants
import kotlin.test.expect

fun forceJavaxXmlStreamParser() {
    StaxParserFactory.factory = { stream, systemId -> JavaxXmlStreamStaxParser.create(stream, systemId) }
}

fun forceXmlpullParser() {
    StaxParserFactory.factory = { stream, systemId -> OrgXmlpullStaxParser.create(stream, systemId) }
}

fun DynaNodeGroup.runOnAllParsers(block: DynaNodeGroup.()->Unit) {
    group("xmlpull") {
        beforeEach { forceXmlpullParser() }
        block()
    }
    group("javax.xml.stream") {
        beforeEach { forceJavaxXmlStreamParser() }
        block()
    }
}

class KonsumerTest : DynaTest({
    runOnAllParsers {
        test("smoke") {
            data class Employee(val id: Int, val name: String, val salary: Int)

            fun employeeXml(k: Konsumer): Employee {
                k.checkCurrent("employee")
                return Employee(k.childInt("id"), k.childText("name"), k.childInt("salary"))
            }

            val employee: Employee = """
            <employee>
                <id>1</id>
                <name>Alba</name>
                <salary>100</salary>
            </employee>
            """.trimIndent().konsumeXml().child("employee") { employeeXml(this) }
            expect(Employee(1, "Alba", 100)) { employee }
        }

        test("current element name") {
            """<employee>
                <id>1</id>
                <name>Alba</name>
            </employee>
            """.konsumeXml().apply {
                expect(null) { name }
                child("employee") {
                    expect("employee") { name!!.toString() }
                    child("id") {
                        expect("id") { name!!.toString() }
                        text()
                    }
                    child("name") {
                        expect("name") { name!!.toString() }
                        text()
                    }
                }
            }
        }

        group("missing elements") {
            lateinit var k: Konsumer
            beforeEach {
                k = """
            <employee>
                <id>1</id>
                <name>Alba</name>
                <salary>100</salary>
            </employee>
            """.konsumeXml()
            }
            test("root") {
                expectThrows(KonsumerException::class, "at null null, in element <null>: Expected element 'foo' but got 'employee'") {
                    k.child("foo") {}
                }
            }
            test("child") {
                expectThrows(KonsumerException::class, "at null null, in element <employee>: Expected element 'name' but got 'id'") {
                    k.child("employee") {
                        child("name") {}
                    }
                }
            }
            test("childOpt") {
                k.child("employee") {
                    expect(null) { childIntOpt("name") }
                    expect(1) { childIntOpt("id") }
                    expect("Alba") { childTextOpt("name") }
                    expect(100) { childIntOpt("salary") }
                }
            }
        }

        group("redundant elements") {
            lateinit var k: Konsumer
            beforeEach {
                k = """
            <employee>
                <id>1</id>
                <name>Alba</name>
                <salary>100</salary>
            </employee>
            """.konsumeXml()
            }
            test("root not fully consumed") {
                expectThrows(KonsumerException::class, "at null null, in element <employee>: Expected END_ELEMENT but got START_ELEMENT: 'id'") {
                    k.child("employee") {}
                }
            }
        }

        group("can't use parent konsumer until child konsumer finishes") {
            test("text()/child()") {
                """<employee>
                <id>1</id>
            </employee>
            """.konsumeXml().apply {
                    val parent = this
                    child("employee") {
                        expectThrows(IllegalStateException::class, "A child konsumer of 'employee' is ongoing, cannot use this consumer of 'null' until the child konsumer finishes") {
                            parent.text()
                        }
                        expectThrows(IllegalStateException::class, "A child konsumer of 'employee' is ongoing, cannot use this consumer of 'null' until the child konsumer finishes") {
                            parent.child("id") {}
                        }
                        expect(1) { childInt("id") }
                    }
                }
            }
            test("attributes") {
                "<employee><id>1</id></employee>".konsumeXml().apply {
                    val parent = this
                    child("employee") {
                        expectThrows(IllegalStateException::class, "A child konsumer of 'employee' is ongoing, cannot use this consumer of 'null' until the child konsumer finishes") {
                            parent.attributes
                        }
                        expect(1) { childInt("id") }
                    }
                }
            }
            test("children()") {
                "<employee><id>1</id></employee>".konsumeXml().apply {
                    val parent = this
                    child("employee") {
                        expectThrows(IllegalStateException::class, "A child konsumer of 'employee' is ongoing, cannot use this consumer of 'null' until the child konsumer finishes") {
                            parent.children("employee") {}
                        }
                        expect(1) { childInt("id") }
                    }
                }
            }
        }

        group("text") {
            group("text conversion") {
                group("toInt()") {
                    test("simple conversion") {
                        expect(1) {
                            """<employee>
                                <id>1</id>
                            </employee>""".konsumeXml().child("employee") {
                                childInt("id")
                            }
                        }
                    }
                    test("failed conversion") {
                        expectThrows(KonsumerException::class, "line 1 column 27 at null null, in element <id>: Failed to convert 'invalid': For input string: \"invalid\"") {
                            """<employee><id>invalid</id></employee>""".konsumeXml().child("employee") {
                                childInt("id")
                            }
                        }
                    }
                    test("failed conversion ?") {
                        expectThrows(KonsumerException::class, "line 1 column 27 at null null, in element <id>: Failed to convert 'invalid': For input string: \"invalid\"") {
                            """<employee><id>invalid</id></employee>""".konsumeXml().child("employee") {
                                childIntOpt("id")
                            }
                        }
                    }
                    test("failed range test") {
                        expectThrows(KonsumerException::class, "line 1 column 21 at null null, in element <id>: Failed to convert '1': The number '1' is not in range 10..15") {
                            """<employee><id>1</id></employee>""".konsumeXml().child("employee") {
                                childInt("id", 10..15)
                            }
                        }
                    }
                    test("failed range test ?") {
                        expectThrows(KonsumerException::class, "line 1 column 21 at null null, in element <id>: Failed to convert '1': The number '1' is not in range 10..15") {
                            """<employee><id>1</id></employee>""".konsumeXml().child("employee") {
                                childIntOpt("id", 10..15)
                            }
                        }
                    }
                    test("range test skipped for missing element") {
                        """<employee/>""".konsumeXml().child("employee") {
                            expect(null) { childIntOpt("id", 10..15) }
                        }
                    }
                }
                group("toLong()") {
                    test("simple conversion") {
                        expect(1) {
                            """<employee>
                                <id>1</id>
                            </employee>""".konsumeXml().child("employee") {
                                childText("id") { it.toLong() }
                            }
                        }
                    }
                    test("failed conversion") {
                        expectThrows(KonsumerException::class, "line 1 column 27 at null null, in element <id>: Failed to convert 'invalid': For input string: \"invalid\"") {
                            """<employee><id>invalid</id></employee>""".konsumeXml().child("employee") {
                                childText("id") { it.toLong() }
                            }
                        }
                    }
                    test("failed conversion ?") {
                        expectThrows(KonsumerException::class, "line 1 column 27 at null null, in element <id>: Failed to convert 'invalid': For input string: \"invalid\"") {
                            """<employee><id>invalid</id></employee>""".konsumeXml().child("employee") {
                                childTextOpt("id") { it.toLong() }
                            }
                        }
                    }
                    test("failed range test") {
                        expectThrows(KonsumerException::class, "line 1 column 21 at null null, in element <id>: Failed to convert '1': The number '1' is not in range 10..15") {
                            """<employee><id>1</id></employee>""".konsumeXml().child("employee") {
                                childText("id") { it.toLong(10L..15) }
                            }
                        }
                    }
                    test("failed range test ?") {
                        expectThrows(KonsumerException::class, "line 1 column 21 at null null, in element <id>: Failed to convert '1': The number '1' is not in range 10..15") {
                            """<employee><id>1</id></employee>""".konsumeXml().child("employee") {
                                childTextOpt("id") { it.toLong(10L..15) }
                            }
                        }
                    }
                    test("range test skipped for missing element") {
                        """<employee/>""".konsumeXml().child("employee") {
                            expect(null) { childTextOpt("id") { it.toLong(10L..15) } }
                        }
                    }
                }
                group("xmlDateTime()") {
                    test("simple conversion") {
                        val dateTime = """<employee><dob>2002-05-30T09:00:00</dob></employee>""".konsumeXml().child("employee") {
                            childText("dob") { it.xmlDateTime() }
                        }
                        expect("2002-05-30T09:00:00") { dateTime.formatXMLDateTime(false) }
                    }
                    test("failed conversion") {
                        expectThrows(KonsumerException::class, "line 1 column 27 at null null, in element <id>: Failed to convert 'invalid': invalid") {
                            """<employee><id>invalid</id></employee>""".konsumeXml().child("employee") {
                                childText("id") { it.xmlDateTime() }
                            }
                        }
                    }
                    test("failed conversion ?") {
                        expectThrows(KonsumerException::class, "line 1 column 27 at null null, in element <id>: Failed to convert 'invalid': invalid") {
                            """<employee><id>invalid</id></employee>""".konsumeXml().child("employee") {
                                childTextOpt("id") { it.xmlDateTime() }
                            }
                        }
                    }
                }
            }
            test("unconsumed") {
                expectThrows(KonsumerException::class, "at null null, in element <e>: Expected END_ELEMENT but there is unconsumed text: 'foo'") {
                    """<e>foo</e>""".konsumeXml().child("e") {}
                }
                expectThrows(KonsumerException::class, "line 1 column 11 at null null, in element <e>: Expected END_ELEMENT but there is unconsumed text: 'foo'") {
                    """<e>foo<b/>bar</e>""".konsumeXml().child("e") { child("b") {} }
                }
            }
        }

        group("children()") {
            test("smoke") {
                """
            <employee>
                <id>1</id>
                <name>Alba</name>
                <salary>100</salary>
            </employee>
            """.trimIndent().konsumeXml().child("employee") {
                    expectList(1) { childrenInt("id") }
                    expectList("Alba") { childrenText("name") }
                    expectList(100) { childrenInt("salary") }
                }
            }
            test("children mixed with text") {
                """
            <employee>
                a <b>bold</b> text with <b>bold</b> contents
            </employee>
            """.trimIndent().konsumeXml().child("employee") {
                    expectList("bold", "bold") { childrenText("b") }
                    expect("atext withcontents") { text() }
                }
            }
            test("multiple children at the start") {
                """<r><a/><a/><a/><b/></r>""".konsumeXml().child("r") {
                    expectList("", "", "") { childrenText("a") }
                    expect("") { childText("b") }
                }
            }
            test("missing children at the start") {
                """<r><a/><a/><a/><b/></r>""".konsumeXml().child("r") {
                    expectList() { childrenText("c") }
                    expectList("", "", "") { childrenText("a") }
                    expect("") { childText("b") }
                }
            }
            test("minCount") {
                expectThrows(KonsumerException::class, "line 1 column 20 at null null, in element <r>: At least 5 of element 'a' was expected, but only 3 encountered") {
                    """<r><a/><a/><a/><b/></r>""".konsumeXml().child("r") {
                        childrenText("a", 5)
                    }
                }
            }
            group("maxCount") {
                test("0 doesn't consume anything") {
                    """<r><a/><a/><a/><b/></r>""".konsumeXml().child("r") {
                        expectList() { childrenText("a", 0, 0) }
                        expectList("") { childrenText("a", 0, 1) }
                        expectList("", "") { childrenText("a", 0, 2) }
                        child("b") {}
                    }
                }
            }
        }

        group("attributes") {
            test("simple") {
                """<e id="5"/>""".konsumeXml().child("e") {
                    expect("5") { attributes["id"] }
                }
            }
            group("toInt()") {
                test("simple") {
                    """<e id="5"/>""".konsumeXml().child("e") {
                        expect(5) { attributes.getValueInt("id") }
                    }
                }
                test("out of range") {
                    expectThrows(KonsumerException::class, "line 1 column 12 at null null, in element <e>: Failed to convert the value '5' of attribute 'id': 'The number '5' is not in range 10..15") {
                        """<e id="5"/>""".konsumeXml().child("e") {
                            attributes.getValueInt("id", 10..15)
                        }
                    }
                }
                test("out of range ?") {
                    expectThrows(KonsumerException::class, "line 1 column 12 at null null, in element <e>: Failed to convert the value '5' of attribute 'id': 'The number '5' is not in range 10..15") {
                        """<e id="5"/>""".konsumeXml().child("e") {
                            attributes.getValueIntOpt("id", 10..15)
                        }
                    }
                }
                test("failed type conversion provides location properly") {
                    expectThrows(KonsumerException::class, "line 1 column 14 at null null, in element <e>: Failed to convert the value 'foo' of attribute 'id': 'For input string: \"foo\"'") {
                        """<e id="foo"/>""".konsumeXml().child("e") {
                            attributes.getValueInt("id")
                        }
                    }
                }
                test("returns null even if outside of range") {
                    """<e/>""".konsumeXml().child("e") {
                        expect(null) { attributes.getValueIntOpt("id", 10..15) }
                    }
                }
            }
            group("toLong()") {
                test("simple") {
                    """<e id="5"/>""".konsumeXml().child("e") {
                        expect(5) { attributes.getValue("id") { it.toLong() } }
                    }
                }
                test("out of range") {
                    expectThrows(KonsumerException::class, "line 1 column 12 at null null, in element <e>: Failed to convert the value '5' of attribute 'id': 'The number '5' is not in range 10..15") {
                        """<e id="5"/>""".konsumeXml().child("e") {
                            attributes.getValue("id") { it.toLong(10L..15) }
                        }
                    }
                }
                test("out of range ?") {
                    expectThrows(KonsumerException::class, "line 1 column 12 at null null, in element <e>: Failed to convert the value '5' of attribute 'id': 'The number '5' is not in range 10..15") {
                        """<e id="5"/>""".konsumeXml().child("e") {
                            attributes.getValueOpt("id") { it.toLong(10L..15) }
                        }
                    }
                }
                test("failed type conversion provides location properly") {
                    expectThrows(KonsumerException::class, "line 1 column 14 at null null, in element <e>: Failed to convert the value 'foo' of attribute 'id': 'For input string: \"foo\"'") {
                        """<e id="foo"/>""".konsumeXml().child("e") {
                            attributes.getValue("id") { it.toLong() }
                        }
                    }
                }
                test("returns null even if outside of range") {
                    """<e/>""".konsumeXml().child("e") {
                        expect(null) { attributes.getValueOpt("id") { it.toLong(10L..15) } }
                    }
                }
            }
            group("missing attribute") {
                test("return null when the attribute is not required") {
                    """<e/>""".konsumeXml().child("e") {
                        expect(null) { attributes.getValueOpt("id") }
                        expect(null) { attributes.getValueIntOpt("id") }
                    }
                }
                test("fail when the attribute is required") {
                    expectThrows(KonsumerException::class, "line 1 column 5 at null null, in element <e>: Required attribute 'id' is missing") {
                        """<e/>""".konsumeXml().child("e") {
                            attributes.getValue("id")
                        }
                    }
                }
            }
            group("unconsumed") {
                test("by default unconsumed attributes are ignored") {
                    """<e id="5"/>""".konsumeXml().child("e") {}
                }
                test("fail on unconsumed attribute") {
                    expectThrows(KonsumerException::class, "line 1 column 12 at null null, in element <e>: The attribute id has not been consumed") {
                        """<e id="5"/>""".konsumeXml().apply {
                            settings.failOnUnconsumedAttributes = true
                            child("e") {}
                        }
                    }
                }
                test("xml:lang") {
                    """<e xml:lang="en"/>""".konsumeXml().child("e") {
                        settings.failOnUnconsumedAttributes = true
                    }
                }
            }
            test("attributes can only be consumed in the beginning") {
                expectThrows(KonsumerException::class, "line 1 column 15 at null null, in element <e>: You can only read element attributes when the element has been freshly entered") {
                    """<e id="5"><a/></e>""".konsumeXml().child("e") {
                        child("a") {}
                        attributes.getValue("id")
                    }
                }
            }
            test("xml:lang") {
                """<e xml:lang="en"/>""".konsumeXml().child("e") {
                    settings.failOnUnconsumedAttributes = true
                    expect("en") { attributes.getValue("lang", XMLConstants.XML_NS_URI) }
                }
            }
        }

        group("stream") {
            test("simple stream") {
                """
                <resources>
                    <string name="name1">content1</string>
                    <string name="name2">content2</string>
                    <string name="name3">content3</string>
                </resources>
                """.trimIndent().konsumeXml().use {
                    val konsumer = it.nextElement("resources", true)!!
                    val resources: Sequence<Resource> = konsumer.childrenSequence("string") { Resource.xml(this) }
                    expect(3) { resources.count() }
                }
            }

            test("lower limit") {
                """<resources></resources>""".konsumeXml().use {
                    val konsumer = it.nextElement("resources", true)!!
                    val resources: Sequence<Resource> = konsumer.childrenSequence("string", 3) { Resource.xml(this) }
                    expectThrows(KonsumerException::class, "at null null, in element <resources>: At least 3 of element 'string' was expected, but only 0 encountered") {
                        resources.count()
                    }
                }
            }

            test("lower limit 2") {
                """<resources><string name="name1">content1</string></resources>""".konsumeXml().use {
                    val konsumer = it.nextElement("resources", true)!!
                    val resources: Sequence<Resource> = konsumer.childrenSequence("string", 3) { Resource.xml(this) }
                    expectThrows(KonsumerException::class, "at null null, in element <resources>: At least 3 of element 'string' was expected, but only 1 encountered") {
                        resources.count()
                    }
                }
            }

            test("lower limit 3") {
                """<resources><string name="name1">content1</string></resources>""".konsumeXml().use {
                    val konsumer = it.nextElement("resources", true)!!
                    val resources: Sequence<Resource> = konsumer.childrenSequence("string", 1) { Resource.xml(this) }
                    expect(1) { resources.count() }
                }
            }
        }

        group("skipContents") {
            test("root empty element") {
                """<a/>""".konsumeXml().use {
                    it.skipContents()
                }
            }
            test("root element") {
                """<a><b/></a>""".konsumeXml().use {
                    it.child("a") { skipContents() }
                }
            }
            test("child empty element") {
                """<a><b/></a>""".konsumeXml().use {
                    it.child("a") {
                        child("b") { skipContents() }
                    }
                }
            }
            test("child element with text") {
                """<a><b>foo</b></a>""".konsumeXml().use {
                    it.child("a") {
                        child("b") { skipContents() }
                    }
                }
            }
            test("child element with elements") {
                """<a><b><c><d>bar</d></c></b></a>""".konsumeXml().use {
                    it.child("a") {
                        child("b") { skipContents() }
                    }
                }
            }
            test("child empty elements") {
                """<a><b/><b/><b/></a>""".konsumeXml().use {
                    it.child("a") {
                        children("b") { skipContents() }
                    }
                }
            }
            test("child elements with text") {
                """<a><b/><b>foo</b><b>bar</b></a>""".konsumeXml().use {
                    it.child("a") {
                        children("b") { skipContents() }
                    }
                }
            }
            test("child empty optional element") {
                """<a/>""".konsumeXml().use {
                    it.child("a") {
                        childOpt("b") { skipContents() }
                    }
                }
                """<a><b/></a>""".konsumeXml().use {
                    it.child("a") {
                        childOpt("b") { skipContents() }
                    }
                }
            }
            test("child optional element with text") {
                """<a><b>foo</b></a>""".konsumeXml().use {
                    it.child("a") {
                        children("b") { skipContents() }
                    }
                }
                """<a/>""".konsumeXml().use {
                    it.child("a") {
                        children("b") { skipContents() }
                    }
                }
            }
        }
        group("special node types") {
            test("character entity") {
                // test for https://gitlab.com/mvysny/konsume-xml/issues/4
                """<a>&#x2117;</a>""".konsumeXml().use {
                    expect("℗") { it.childText("a") }
                }
            }
            test("Comment") {
                """<a><!-- ignored --></a>""".konsumeXml().use {
                    expect("") { it.childText("a") }
                }
            }
            test("CDATA") {
                """<a><![CDATA[foo]]></a>""".konsumeXml().use {
                    expect("foo") { it.childText("a") }
                }
            }
            test("PI") {
                """<a><?PITarget PIContent?></a>""".konsumeXml().use {
                    expect("") { it.childText("a") }
                }
            }
        }

        group("exceptions wrapped properly") {
            test("simple root element") {
                expectThrows(KonsumerException::class, "line 1 column 5 at null null, in element <a>: simulated") {
                    """<a/>""".konsumeXml().child("a") { throw RuntimeException("simulated") }
                }
                expectThrows(KonsumerException::class, "line 1 column 5 at null null, in element <a>: simulated") {
                    """<a/>""".konsumeXml().children("a") { throw RuntimeException("simulated") }
                }
                expectThrows(KonsumerException::class, "line 1 column 5 at null null, in element <a>: simulated") {
                    """<a/>""".konsumeXml().childOpt("a") { throw RuntimeException("simulated") }
                }
            }
        }
    }
})
